package ua.edu.onu.DepartmentPublications.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ua.edu.onu.DepartmentPublications.dao.model.Faculty;

import javax.swing.text.View;
import java.util.List;
import java.util.Optional;

public interface FacultyRepository extends CrudRepository<Faculty, Long>, JpaSpecificationExecutor<Faculty> {
    Optional<Faculty> findByFacultyName(String facultyName);
    @Query("select f.facultyName from Faculty f where f.facultyId = :facultyId")
    String getFacultyNameByFacultyId(@Param("facultyId") Long facultyId);
    @Query("select f.facultyId from Faculty f where f.facultyName = :name")
    Long getFacultyIdByFacultyName(@Param("name") String facultyName);


    @Query(value = "SELECT t.* FROM public.science_publications t", nativeQuery = true)
    List<String> getDepartmentsWithLessNumberOfSciencePublications();

}
