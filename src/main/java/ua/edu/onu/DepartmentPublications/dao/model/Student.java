package ua.edu.onu.DepartmentPublications.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "student")
@ToString
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id", nullable = false, unique = true)
    private Long studentId;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "speciality_id", nullable = false)
    private Long specialityId;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "tel_no", nullable = false)
    private String telNo;

    @Column(name = "additional")
    private String additional;

    @JsonFormat(pattern="dd-MM-yyyy")
    @Column(name = "study_begin", nullable = false)
    private Date studyBegin;

    @JsonFormat(pattern="dd-MM-yyyy")
    @Column(name = "study_end")
    private Date studyEnd;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "speciality_id", referencedColumnName = "speciality_id", nullable = false, insertable = false, updatable = false)
    Speciality speciality = new Speciality();
}
