package ua.edu.onu.DepartmentPublications.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import ua.edu.onu.DepartmentPublications.dao.model.PublicationLecturer;

import java.util.List;
import java.util.Optional;

public interface PublicationLecturerRepository extends CrudRepository<PublicationLecturer, Long>, JpaSpecificationExecutor<PublicationLecturer> {
}
