package ua.edu.onu.DepartmentPublications.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ua.edu.onu.DepartmentPublications.dao.model.Department;

import java.util.Optional;

public interface DepartmentRepository extends CrudRepository<Department, Long>, JpaSpecificationExecutor<Department> {
    Optional<Department> findDepartmentByDepartmentName(String departmentName);
    @Query("select d.departmentId from Department d where d.departmentName = :name")
    Long getDepartmentIdByDepartmentName(@Param("name") String departmentName);
    @Query("select d.departmentName from Department d where d.departmentId = :id")
    String getDepartmentNameByDepartmentId(@Param("id") Long departmentId);
}
