package ua.edu.onu.DepartmentPublications.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import ua.edu.onu.DepartmentPublications.dao.model.Student;

import java.util.Optional;

public interface StudentRepository extends CrudRepository<Student, Long>, JpaSpecificationExecutor<Student> {
    Optional<Student> findByLastName(String lastName);
}
