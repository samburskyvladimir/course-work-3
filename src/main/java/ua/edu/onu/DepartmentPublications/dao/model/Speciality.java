package ua.edu.onu.DepartmentPublications.dao.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "speciality")
@ToString
public class Speciality {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "speciality_id", nullable = false, unique = true)
    private Long specialityId;

    @Column(name = "speciality_name", nullable = false, unique = true)
    private String specialityName;

    @OneToMany(mappedBy = "speciality")
    List<Subject> subjects = new ArrayList<>();

    @OneToMany(mappedBy = "speciality")
    List<Student> students = new ArrayList<>();
}
