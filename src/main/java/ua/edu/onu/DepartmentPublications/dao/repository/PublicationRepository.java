package ua.edu.onu.DepartmentPublications.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ua.edu.onu.DepartmentPublications.dao.model.Publication;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface PublicationRepository extends CrudRepository<Publication, Long>, JpaSpecificationExecutor<Publication> {
    Optional<Publication> findByPublicationTitle(String publicationTitle);
    void deleteByPublicationDateBetweenAndPageNumbIsLessThan(Date begin, Date end, Integer numb);

    @Query(value = "Select sub.subject_name, count(sub.subject_name)\n" +
            "            from publications as pub, subject as sub\n" +
            "            where sub.subject_id = pub.subject_id\n" +
            "            and (pub.publication_use similar to ('%[М|м]етодическ[ие|ое]%'))\n" +
            "            group by sub.subject_name\n" +
            "            order by count(sub.subject_name) desc limit 3;", nativeQuery = true)
    List<String> getMethodicalPublicationsForEachSubjectDesc();
}
