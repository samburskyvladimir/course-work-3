package ua.edu.onu.DepartmentPublications.dao.repository.special;

import org.springframework.data.jpa.domain.Specification;
import ua.edu.onu.DepartmentPublications.dao.model.Department;
import ua.edu.onu.DepartmentPublications.dao.model.Lecturer;

public class LecturerSpecial {

    public static Specification<Lecturer> withFirstName(String lecturerName){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(lecturerName != null) {
                return criteriaBuilder.equal(root.get("firstName"), lecturerName);
            }
            return null;
        };
    }

    public static Specification<Lecturer> withLastName(String lecturerSurname){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(lecturerSurname != null) {
                return criteriaBuilder.like(root.get("lastName"), "%" + lecturerSurname + "%");
            }
            return null;
        };
    }

    public static Specification<Lecturer> withDepartmentName(Department department) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(department != null && department.getDepartmentName() != null) {
                return criteriaBuilder.equal(root.get("departmentName"), department.getDepartmentName().toUpperCase());
            }
            return null;
        };
    }

    public static Specification<Lecturer> withPost(String lecturerPost){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(lecturerPost != null) {
                return criteriaBuilder.equal(root.get("post"), lecturerPost);
            }
            return null;
        };
    }

    public static Specification<Lecturer> withRank(String lecturerRank){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(lecturerRank != null) {
                return criteriaBuilder.equal(root.get("rank"), lecturerRank);
            }
            return null;
        };
    }

    public static Specification<Lecturer> buildSearchSpec(String lecturerName, String lecturerSurname, Department department, String lecturerPost, String lecturerRank) {
        return Specification
                .where(withFirstName(lecturerName))
                .and(withLastName(lecturerSurname))
                .and(withDepartmentName(department))
                .and(withPost(lecturerPost))
                .and(withRank(lecturerRank));
    }

}
