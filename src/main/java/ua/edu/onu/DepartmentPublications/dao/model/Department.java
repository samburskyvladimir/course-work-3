package ua.edu.onu.DepartmentPublications.dao.model;

import lombok.*;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "department")
@ToString
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "department_id", unique = true)
    private Long departmentId;

    @Column(name = "department_name", nullable = false, unique = true)
    private String departmentName;

    @Column(name = "department_head")
    private Long departmentHead;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "fac_id", referencedColumnName = "faculty_id", nullable = false)
    Faculty faculty;

    @OneToMany(mappedBy = "department")
    List<Lecturer> lecturers = new ArrayList<>();
}
