package ua.edu.onu.DepartmentPublications.dao.repository.special;

import org.springframework.data.jpa.domain.Specification;
import ua.edu.onu.DepartmentPublications.dao.model.Speciality;
import ua.edu.onu.DepartmentPublications.dao.model.Student;

import java.util.Date;

public class StudentSpecial {

    public static Specification<Student> withFirstName(String studentName){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(studentName != null) {
                return criteriaBuilder.equal(root.get("firstName"), studentName);
            }
            return null;
        };
    }

    public static Specification<Student> withLastName(String studentSurname){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(studentSurname != null) {
                return criteriaBuilder.like(root.get("lastName"), "%" + studentSurname + "%");
            }
            return null;
        };
    }

    public static Specification<Student> withSpecialityName(Speciality speciality) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(speciality != null && speciality.getSpecialityName() != null) {
                return criteriaBuilder.equal(root.get("specialityName"), speciality.getSpecialityName().toUpperCase());
            }
            return null;
        };
    }

    public static Specification<Student> withAdditional(String additional){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(additional != null) {
                return criteriaBuilder.like(root.get("additional"), "%" + additional + "%");
            }
            return null;
        };
    }

    public static Specification<Student> withEmail(String email){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(email != null) {
                return criteriaBuilder.like(root.get("email"), "%" + email + "%");
            }
            return null;
        };
    }

    public static Specification<Student> withDateOfStudyBegin(Date begin){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (begin != null ) {
                return criteriaBuilder.equal(root.get("studyBegin"), begin);
            }
            return null;
        };
    }

    public static Specification<Student> withDateOfStudyEnd(Date end){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (end != null ) {
                return criteriaBuilder.equal(root.get("studyEnd"), end);
            }
            return null;
        };
    }

    public static Specification<Student> buildSearchSpec(String studentName, String studentSurname, Speciality speciality, String additional, String email, Date begin, Date end) {
        return Specification
                .where(withFirstName(studentName))
                .and(withLastName(studentSurname))
                .and(withSpecialityName(speciality))
                .and(withAdditional(additional))
                .and(withDateOfStudyBegin(begin))
                .and(withDateOfStudyEnd(end))
                .and(withEmail(email));
    }

}
