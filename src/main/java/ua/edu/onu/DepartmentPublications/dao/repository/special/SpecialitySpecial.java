package ua.edu.onu.DepartmentPublications.dao.repository.special;

import org.springframework.data.jpa.domain.Specification;
import ua.edu.onu.DepartmentPublications.dao.model.Speciality;
import ua.edu.onu.DepartmentPublications.dao.model.Subject;

public class SpecialitySpecial {

    public static Specification<Speciality> withSpecialityName(String specialityName){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(specialityName != null) {
                return criteriaBuilder.like(root.get("specialityName"), "%" + specialityName.toUpperCase() + "%");
            }
            return null;
        };
    }

    public static Specification<Speciality> withSubjectName(Subject subject){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (subject != null && subject.getSubjectName() != null) {
                return criteriaBuilder.like(root.get("subjectName"), "%" + subject.getSubjectName() + "%");
            }
            return null;
        };
    }

    public static Specification<Speciality> buildSearchSpec(String specialityName, Subject subject) {
        return Specification
                .where(withSpecialityName(specialityName))
                .and(withSubjectName(subject));
    }
}
