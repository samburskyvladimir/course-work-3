package ua.edu.onu.DepartmentPublications.dao.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lecturer")
@ToString
public class Lecturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lecturer_id", nullable = false, unique = true)
    private Long lecturerId;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "post", nullable = false)
    private String post;

    @Column(name = "lecturer_rank")
    private String rank;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "department_id", referencedColumnName = "department_id", nullable = false)
    Department department;

    @OneToMany(mappedBy = "lecturer")
    List<PublicationLecturer> publicationLecturers = new ArrayList<>();
}
