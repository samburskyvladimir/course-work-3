package ua.edu.onu.DepartmentPublications.dao.repository.special;

import org.springframework.data.jpa.domain.Specification;
import ua.edu.onu.DepartmentPublications.dao.model.Subject;

public class SubjectSpecial {

    public static Specification<Subject> withSubjectName(String subjectName){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(subjectName != null) {
                return criteriaBuilder.like(root.get("specialityName"), "%" + subjectName + "%");
            }
            return null;
        };
    }

    public static Specification<Subject> buildSearchSpec(String subjectName) {
        return Specification
                .where(withSubjectName(subjectName));
    }

}
