package ua.edu.onu.DepartmentPublications.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ua.edu.onu.DepartmentPublications.dao.model.Lecturer;

import java.util.List;
import java.util.Optional;

public interface LecturerRepository extends CrudRepository<Lecturer, Long>, JpaSpecificationExecutor<Lecturer> {
    Optional<Lecturer> findByFirstName(String firstName);
    Optional<Lecturer> findByLastName(String lastName);
    Optional<Lecturer> findByEmail(String email);
    @Query("select l.firstName from Lecturer l where l.lecturerId = :id")
    String getLecturerFirstNameByLecturerId(@Param("id") Long lecturerId);
    @Query("select l.lastName from Lecturer l where l.lecturerId = :id")
    String getLecturerLastNameByLecturerId(@Param("id") Long lecturerId);
    @Query("select l.patronymic from Lecturer l where l.lecturerId = :id")
    String getLecturerPatronymicByLecturerId(@Param("id") Long lecturerId);
    @Query("select l.email from Lecturer l where l.lecturerId = :id")
    String getLecturerEmailByLecturerId(@Param("id") Long lecturerId);
    @Query("select l.lecturerId from Lecturer l where l.email = :email")
    Long getLecturerIdByLecturerEmail(@Param("email") String email);

    @Query(value = "select l.\"last_name\"" +
            "\t\tfrom lecturer as l, department as d, faculty as f\n" +
            "\n" +
            " where l.\"department_id\" = d.\"department_id\" \n" +
            "\t\tand d.\"fac_id\" = f.\"faculty_id\" \n" +
            "\t\tand f.\"faculty_name\" = ?1 \n" +
            "\tgroup by (l.\"last_name\") having count(*) > 1;", nativeQuery = true)
    List<String> getLecturerWithSimilarSurnamesOnFaculty(String facultyName);

    @Query(value = "with publications_general as (\n" +
            "    select lec.lecturer_id, count(publ.publication_id) as first, pub.publication_id\n" +
            "    from lecturer as lec, department as d, publication_lecturer as publ, publications as pub\n" +
            "    where lec.department_id = d.department_id\n" +
            "      and d.department_name = ?1\n" +
            "      and publ.lecturer_id = lec.lecturer_id and publ.publication_id = pub.publication_id\n" +
            "      and extract(year from current_date) - extract(year from pub.publication_date) = 0\n" +
            "    group by lec.lecturer_id, pub.publication_id\n" +
            "), materials_count as(\n" +
            "    select l.lecturer_id, count(pl.publication_id) as second, pub.publication_id\n" +
            "    from lecturer as l, department as d, publication_lecturer as pl, publications as pub\n" +
            "    where l.department_id = d.department_id\n" +
            "      and d.department_name = ?1\n" +
            "      and pl.lecturer_id = l.lecturer_id and pl.publication_id = pub.publication_id\n" +
            "      and pub.publication_use not in ('Научное издание','Научно-популярное издание')\n" +
            "      and extract(year from current_date) - extract(year from pub.publication_date) = 0\n" +
            "    group by l.lecturer_id, pub.publication_id)\n" +
            "    select l.last_name, pub.publication_title, pub.publication_date, pub.publication_use,\n" +
            "    dense_rank() over(order by first) as \"rank1\",\n" +
            "    dense_rank() over(order by second) as \"rank2\" from publications_general\n" +
            "                                                   join lecturer as l using(lecturer_id)\n" +
            "                                                   join publications as pub using (publication_id)\n" +
            "                                                   left join materials_count using(lecturer_id)", nativeQuery = true)
    List<String> getLecturersInRangePublicationsGeneralAndTeachingPublications(String departmentName);

    @Query(value = "with pub_data1 as(\n" +
            "    select l.lecturer_id, count(pub.publication_id) count_1\n" +
            "    from publications as pub\n" +
            "             join  publication_lecturer as pl on(pl.publication_id = pub.publication_id)\n" +
            "             join lecturer as l on(l.lecturer_id = pl.lecturer_id)\n" +
            "    where\n" +
            "            pub.publication_use = 'Научное издание' or pub.publication_type = 'Научно-популярное издание'\n" +
            "    group by l.lecturer_id\n" +
            "), pub_data2 as(\n" +
            "    select l.lecturer_id, count(pub.publication_id) count_2\n" +
            "    from publications as pub\n" +
            "             join  publication_lecturer as pl on(pl.publication_id = pub.publication_id)\n" +
            "             join lecturer as l on(l.lecturer_id = pl.lecturer_id)\n" +
            "    where\n" +
            "            pub.publication_use not in ('Научное издание', 'Научно-популярное издание')\n" +
            "    group by l.lecturer_id\n" +
            ")\n" +
            "select l.last_name, l.first_name, l.patronymic, count_1, count_2 from lecturer as l\n" +
            "                                                left join pub_data1 using(lecturer_id)\n" +
            "                                                left join pub_data2 on(pub_data2.lecturer_id = l.lecturer_id)\n" +
            "where (case when count_1 is null then 0 else count_1 end) > (case when count_2 is null then 0 else count_2 end)", nativeQuery = true)
    List<String> getLecturersHaveSciencePublicationsNumbTwiceBiggerThanTeachingPublications();
}
