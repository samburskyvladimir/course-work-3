package ua.edu.onu.DepartmentPublications.dao.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "publications")
@ToString
public class Publication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "publication_id", nullable = false, unique = true)
    private Long publicationId;

    @JsonFormat(pattern="dd-MM-yyyy")
    @Column(name = "publication_date", nullable = false)
    private Date publicationDate;

    @Column(name = "publication_city", nullable = false)
    private String publicationCity;

    @Column(name = "download_link")
    private String downloadLink;

    @Column(name = "publication_use", nullable = false)
    private String publicationUse;

    @Column(name = "publication_type", nullable = false)
    private String publicationType;

    @Column(name = "udc_numb", nullable = false)
    private Integer udcNumb;

    @Column(name = "isbn_numb", nullable = false)
    private Integer isbnNumb;

    @Column(name = "page_numb", nullable = false)
    private Integer pageNumb;

    @Column(name = "publication_title", nullable = false)
    private String publicationTitle;

    @OneToMany(mappedBy = "publication")
    List<PublicationLecturer> publicationLecturers = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "subject_id", referencedColumnName = "subject_id", nullable = false)
    Subject subject = new Subject();
}
