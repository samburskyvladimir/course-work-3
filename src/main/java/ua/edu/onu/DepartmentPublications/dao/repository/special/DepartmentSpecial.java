package ua.edu.onu.DepartmentPublications.dao.repository.special;

import org.springframework.data.jpa.domain.Specification;
import ua.edu.onu.DepartmentPublications.dao.model.Department;
import ua.edu.onu.DepartmentPublications.dao.model.Faculty;
import ua.edu.onu.DepartmentPublications.dao.model.Lecturer;

public class DepartmentSpecial {

    public static Specification<Department> withDepartmentName(String departmentName) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(departmentName != null) {
                return criteriaBuilder.like(root.get("departmentName"), "%" + departmentName.toUpperCase() + "%");
            }
            return null;
        };
    }

    public static Specification<Department> withHeadName(Lecturer lecturer){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(lecturer != null && lecturer.getFirstName() != null) {
                return criteriaBuilder.equal(root.get("firstName"), lecturer.getFirstName());
            }
            return null;
        };
    }

    public static Specification<Department> withFacultyName(Faculty faculty){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(faculty != null && faculty.getFacultyName() != null) {
                return criteriaBuilder.equal(root.get("facultyName"), faculty.getFacultyName().toUpperCase());
            }
            return null;
        };
    }



    public static Specification<Department> buildSearchSpec(String departmentName, Lecturer lecturer, Faculty faculty) {
        return Specification
                .where(withDepartmentName(departmentName))
                .and(withHeadName(lecturer))
                .and(withFacultyName(faculty));
    }
}
