package ua.edu.onu.DepartmentPublications.dao.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "faculty")
@ToString
public class Faculty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "faculty_id", nullable = false, unique = true)
    private Long facultyId;

    @Column(name = "faculty_name", nullable = false, unique = true)
    private String facultyName;

    @OneToMany(mappedBy = "faculty")
    List<Department> departments = new ArrayList<>();
}
