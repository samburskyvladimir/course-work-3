package ua.edu.onu.DepartmentPublications.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ua.edu.onu.DepartmentPublications.dao.model.Subject;

import java.util.Optional;

public interface SubjectRepository extends CrudRepository<Subject, Long>, JpaSpecificationExecutor<Subject> {
    Optional<Subject> findBySubjectName(String subjectName);

    @Query("select s.subjectId from Subject s where s.subjectName like('%:name%')")
    Long getSubjectIdBySubjectName(@Param("name") String subjectName);

    @Query("select s.subjectName from Subject s where s.subjectId = :id")
    String getSubjectNameBySubjectId(@Param("id") Long subjectId);
}
