package ua.edu.onu.DepartmentPublications.dao.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ua.edu.onu.DepartmentPublications.dao.model.Speciality;

import java.util.Optional;

public interface SpecialityRepository extends CrudRepository<Speciality, Long>, JpaSpecificationExecutor<Speciality> {
    Optional<Speciality> findSpecialityBySpecialityName(String specialityName);
    @Query("select s.specialityId from Speciality s where s.specialityName = :name")
    Long getSpecialityIdBySpecialityName(@Param("name") String specialityName);
    @Query("select s.specialityName from Speciality s where s.specialityId = :id")
    String getSpecialityNameBySpecialityId(@Param("id") Long specialityId);
}
