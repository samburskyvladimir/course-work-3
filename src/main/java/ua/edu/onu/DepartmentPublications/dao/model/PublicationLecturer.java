package ua.edu.onu.DepartmentPublications.dao.model;

import lombok.*;
import ua.edu.onu.DepartmentPublications.dao.CompoundKeys.PublicationLecturerPKey;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "publication_lecturer")
@ToString
public class PublicationLecturer {

    @EmbeddedId
    private PublicationLecturerPKey id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "lecturer_id", referencedColumnName = "lecturer_id", insertable = false, updatable = false)
    private Lecturer lecturer;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "publication_id", referencedColumnName = "publication_id", insertable = false, updatable = false)
    private Publication publication;
}
