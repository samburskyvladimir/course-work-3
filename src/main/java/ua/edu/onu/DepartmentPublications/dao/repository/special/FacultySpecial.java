package ua.edu.onu.DepartmentPublications.dao.repository.special;

import org.springframework.data.jpa.domain.Specification;
import ua.edu.onu.DepartmentPublications.dao.model.Faculty;
import ua.edu.onu.DepartmentPublications.dao.model.Lecturer;

public class FacultySpecial {

    public static Specification<Faculty> withFacultyName(String facultyName) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(facultyName != null) {
                return criteriaBuilder.like(root.get("facultyName"), "%" + facultyName.toUpperCase() + "%");
            }
            return null;
        };
    }

    public static Specification<Faculty> buildSearchSpec(String facultyName) {
        return Specification
                .where(withFacultyName(facultyName));
    }
}
