package ua.edu.onu.DepartmentPublications.dao.CompoundKeys;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PublicationLecturerPKey implements Serializable {

    @Column(name = "publication_id", nullable = false)
    private Long publicationId;

    @Column(name = "lecturer_id", nullable = false)
    private Long lecturerId;
}
