package ua.edu.onu.DepartmentPublications.dao.repository.special;

import org.springframework.data.jpa.domain.Specification;
import ua.edu.onu.DepartmentPublications.dao.model.*;

public class PublicationLecturerSpecial {

    public static Specification<PublicationLecturer> withLecturerSurname(Lecturer lecturer){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(lecturer != null && lecturer.getLastName() != null) {
                return criteriaBuilder.equal(root.get("lastName"), lecturer.getLastName());
            }
            return null;
        };
    }

    public static Specification<PublicationLecturer> withPublicationTitle(Publication publication){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if(publication != null && publication.getPublicationTitle() != null) {
                return criteriaBuilder.like(root.get("publicationTitle"), "%" + publication.getPublicationTitle() + "%");
            }
            return null;
        };
    }

    public static Specification<PublicationLecturer> buildSearchSpec(Lecturer lecturer, Publication publication) {
        return Specification
                .where(withPublicationTitle(publication))
                .and(withLecturerSurname(lecturer));
    }
}
