package ua.edu.onu.DepartmentPublications.dao.repository.special;

import org.springframework.data.jpa.domain.Specification;
import ua.edu.onu.DepartmentPublications.dao.model.Department;
import ua.edu.onu.DepartmentPublications.dao.model.Lecturer;
import ua.edu.onu.DepartmentPublications.dao.model.Publication;
import ua.edu.onu.DepartmentPublications.dao.model.Subject;

import java.util.Date;

public class PublicationSpecial {

    public static Specification<Publication> withDateBetween(Date begin, Date end){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (begin != null && end != null) {
                return criteriaBuilder.between(root.get("publicationDate"), begin, end);
            }
            return null;
        };
    }

    public static Specification<Publication> withPublicationCity(String city){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (city != null) {
                return criteriaBuilder.equal(root.get("publicationCity"), city);
            }
            return null;
        };
    }

    public static Specification<Publication> withPublicationUse(String use){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (use != null) {
                return criteriaBuilder.like(root.get("publicationUse"), "%" + use + "%");
            }
            return null;
        };
    }

    public static Specification<Publication> withPublicationTitle(String title){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (title != null) {
                return criteriaBuilder.like(root.get("publicationTitle"), "%" + title + "%");
            }
            return null;
        };
    }

    public static Specification<Publication> withPageNumb(Long numbBegin, Long numbEnd){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (numbBegin != null && numbEnd != null) {
                return criteriaBuilder.between(root.get("pageNumb"), numbBegin, numbEnd);
            }
            return null;
        };
    }

    public static Specification<Publication> withPublicationType(String type){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (type != null) {
                return criteriaBuilder.like(root.get("publicationType"), "%" + type + "%");
            }
            return null;
        };
    }

    public static Specification<Publication> withAuthorSurname(Lecturer lecturer){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (lecturer != null && lecturer.getLastName() != null) {
                return criteriaBuilder.equal(root.get("lastName"), lecturer.getLastName());
            }
            return null;
        };
    }

    public static Specification<Publication> withSubjectName(Subject subject){
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (subject != null && subject.getSubjectName() != null) {
                return criteriaBuilder.like(root.get("subjectName"), "%" + subject.getSubjectName() + "%");
            }
            return null;
        };
    }

    public static Specification<Publication> buildSearchSpec(Date begin, Date end, String city, String use, String title, Long numbBegin, Long numbEnd, String type, Lecturer lecturer, Subject subject) {
        return Specification
                .where(withDateBetween(begin, end))
                .and(withPageNumb(numbBegin, numbEnd))
                .and(withPublicationCity(city))
                .and(withPublicationTitle(title))
                .and(withPublicationType(type))
                .and(withPublicationUse(use))
                .and(withAuthorSurname(lecturer))
                .and(withSubjectName(subject));
    }
}
