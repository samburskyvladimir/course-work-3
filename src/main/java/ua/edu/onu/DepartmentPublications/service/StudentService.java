package ua.edu.onu.DepartmentPublications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.onu.DepartmentPublications.dao.model.Speciality;
import ua.edu.onu.DepartmentPublications.dao.model.Student;
import ua.edu.onu.DepartmentPublications.dao.repository.SpecialityRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.StudentRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.special.StudentSpecial;
import ua.edu.onu.DepartmentPublications.dto.StudentDto;
import ua.edu.onu.DepartmentPublications.exception.NotFoundException;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
public class StudentService {

    private StudentRepository studentRepository;
    private SpecialityRepository specialityRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository, SpecialityRepository specialityRepository) {
        this.studentRepository = studentRepository;
        this.specialityRepository = specialityRepository;
    }

    private final Function<Student, StudentDto> studentToDto = entity -> StudentDto.builder()
            .studentId(entity.getStudentId())
            .additional(entity.getAdditional())
            .email(entity.getEmail())
            .firstName(entity.getFirstName())
            .lastName(entity.getLastName())
            .patronymic(entity.getPatronymic())
            .specialityName(specialityRepository.getSpecialityNameBySpecialityId(entity.getSpecialityId()))
            .studyBegin(entity.getStudyBegin())
            .studyEnd(entity.getStudyEnd())
            .telNo(entity.getTelNo())
            .build();

    private final Function<StudentDto, Student> dtoToStudent = dto -> Student.builder()
            .studentId(dto.getStudentId())
            .additional(dto.getAdditional())
            .email(dto.getEmail())
            .firstName(dto.getFirstName())
            .lastName(dto.getLastName())
            .patronymic(dto.getPatronymic())
            .specialityId(specialityRepository.getSpecialityIdBySpecialityName(dto.getSpecialityName()))
            .studyBegin(dto.getStudyBegin())
            .studyEnd(dto.getStudyEnd())
            .telNo(dto.getTelNo())
            .build();

    public List<StudentDto> list() {
        List<StudentDto> response = new LinkedList<>();
        studentRepository.findAll().forEach(student -> response.add(studentToDto.apply(student)));
        return response;
    }

    public StudentDto create(StudentDto studentDto) {
        StudentDto response = studentToDto.apply(studentRepository.save(dtoToStudent.apply(studentDto)));
        return response;
    }

    public List<StudentDto> find(String studentName, String studentSurname, String specialityName, String additional, String email, Date begin, Date end){
        List<StudentDto> response = new LinkedList<>();
        Speciality speciality = specialityRepository.findSpecialityBySpecialityName(specialityName).orElse(null);
        studentRepository.findAll(StudentSpecial.buildSearchSpec(studentName, studentSurname, speciality, additional, email, begin, end)).forEach(student -> response.add(studentToDto.apply(student)));
        return response;
    }

    public void update(StudentDto studentDto) {
        Student student = studentRepository.findById(studentDto.getStudentId())
                .orElseThrow(() -> NotFoundException.notFoundWhenUpdate(Student.class));
        student.setLastName(studentDto.getLastName());
        student.setAdditional(studentDto.getAdditional());
        student.setFirstName(studentDto.getFirstName());
        student.setEmail(studentDto.getEmail());
        student.setPatronymic(studentDto.getPatronymic());
        student.setTelNo(studentDto.getTelNo());
        student.setStudyBegin(studentDto.getStudyBegin());
        student.setSpeciality(specialityRepository.findSpecialityBySpecialityName(studentDto.getSpecialityName()).orElseThrow(()
                -> NotFoundException.returnNotFoundEntity(studentDto, Speciality.class.getName())));
        studentRepository.save(student);
    }

    public void delete(StudentDto studentDto) {
        studentRepository.findById(studentDto.getStudentId())
                .orElseThrow(() -> NotFoundException.notFoundWhenDelete(Student.class));
        studentRepository.deleteById(studentDto.getStudentId());
    }

}
