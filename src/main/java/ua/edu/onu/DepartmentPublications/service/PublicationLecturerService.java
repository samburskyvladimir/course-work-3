package ua.edu.onu.DepartmentPublications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.onu.DepartmentPublications.dao.CompoundKeys.PublicationLecturerPKey;
import ua.edu.onu.DepartmentPublications.dao.model.Lecturer;
import ua.edu.onu.DepartmentPublications.dao.model.Publication;
import ua.edu.onu.DepartmentPublications.dao.model.PublicationLecturer;
import ua.edu.onu.DepartmentPublications.dao.repository.LecturerRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.PublicationLecturerRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.PublicationRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.special.PublicationLecturerSpecial;
import ua.edu.onu.DepartmentPublications.dto.PublicationLecturerDto;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
public class PublicationLecturerService {

    private PublicationLecturerRepository publicationLecturerRepository;
    private PublicationRepository publicationRepository;
    private LecturerRepository lecturerRepository;

    @Autowired
    public PublicationLecturerService(PublicationLecturerRepository publicationLecturerRepository,
                                      PublicationRepository publicationRepository,
                                      LecturerRepository lecturerRepository) {
        this.publicationLecturerRepository = publicationLecturerRepository;
        this.lecturerRepository = lecturerRepository;
        this.publicationRepository = publicationRepository;
    }

    private final Function<PublicationLecturer, PublicationLecturerDto> publicationLecturerToDto = entity -> PublicationLecturerDto.builder()
            .publicationId(entity.getPublication().getPublicationId())
            .publicationTitle(entity.getPublication().getPublicationTitle())
            .publicationType(entity.getPublication().getPublicationType())
            .publicationUse(entity.getPublication().getPublicationUse())
            .publicationSubject(entity.getPublication().getSubject().getSubjectName())
            .lecturerEmail(entity.getLecturer().getEmail())
            .lecturerPatronymic(entity.getLecturer().getPatronymic())
            .lecturerName(entity.getLecturer().getFirstName())
            .lecturerSurname(entity.getLecturer().getLastName())
            .departmentName(entity.getLecturer().getDepartment().getDepartmentName())
            .build();

    private final Function<PublicationLecturerDto, PublicationLecturer> dtoToPublicationLecturer = dto -> PublicationLecturer.builder()
            .id(new PublicationLecturerPKey(dto.getPublicationId(), lecturerRepository.getLecturerIdByLecturerEmail(dto.getLecturerEmail())))
            .lecturer(lecturerRepository.findByEmail(dto.getLecturerEmail()).orElse(null))
            .publication(publicationRepository.findById(dto.getPublicationId()).orElse(null))
            .build();
    public List<PublicationLecturerDto> list() {
        List<PublicationLecturerDto> response = new LinkedList<>();
        publicationLecturerRepository.findAll().forEach(publicationLecturer -> response.add(publicationLecturerToDto.apply(publicationLecturer)));
        return response;
    }

    public PublicationLecturerDto create(PublicationLecturerDto publicationLecturerDto) {
        PublicationLecturerDto response = publicationLecturerToDto.apply(publicationLecturerRepository.save(dtoToPublicationLecturer.apply(publicationLecturerDto)));
        return response;
    }

    public List<PublicationLecturerDto> find(String lecturerSurname, String publicationTitle){
        List<PublicationLecturerDto> response = new LinkedList<>();
        Publication publication = publicationRepository.findByPublicationTitle(publicationTitle).orElse(null);
        Lecturer lecturer = lecturerRepository.findByLastName(lecturerSurname).orElse(null);
        publicationLecturerRepository.findAll(PublicationLecturerSpecial.buildSearchSpec(lecturer, publication)).forEach(publicationLecturer -> response.add(publicationLecturerToDto.apply(publicationLecturer)));
        return response;
    }
}
