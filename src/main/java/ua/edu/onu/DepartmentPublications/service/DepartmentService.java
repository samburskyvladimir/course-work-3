package ua.edu.onu.DepartmentPublications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.onu.DepartmentPublications.dao.model.Department;
import ua.edu.onu.DepartmentPublications.dao.model.Faculty;
import ua.edu.onu.DepartmentPublications.dao.model.Lecturer;
import ua.edu.onu.DepartmentPublications.dao.repository.DepartmentRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.FacultyRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.LecturerRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.special.DepartmentSpecial;
import ua.edu.onu.DepartmentPublications.dto.DepartmentDto;
import ua.edu.onu.DepartmentPublications.exception.NotFoundException;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
public class DepartmentService {

    private DepartmentRepository departmentRepository;
    private FacultyRepository facultyRepository;
    private LecturerRepository lecturerRepository;
    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository, FacultyRepository facultyRepository, LecturerRepository lecturerRepository) {
        this.departmentRepository = departmentRepository;
        this.facultyRepository = facultyRepository;
        this.lecturerRepository = lecturerRepository;
    }

    private final Function<Department, DepartmentDto> departmentToDto = entity -> DepartmentDto.builder()
            .departmentName(entity.getDepartmentName())
            .facultyName(facultyRepository.getFacultyNameByFacultyId(entity.getFaculty().getFacultyId()))
            .lecturerFirstName(lecturerRepository.getLecturerFirstNameByLecturerId(entity.getDepartmentHead()))
            .lecturerLastName(lecturerRepository.getLecturerLastNameByLecturerId(entity.getDepartmentHead()))
            .lecturerPatronymic(lecturerRepository.getLecturerPatronymicByLecturerId(entity.getDepartmentHead()))
            .lecturerEmail(lecturerRepository.getLecturerEmailByLecturerId(entity.getDepartmentHead()))
            .build();

    private final Function<DepartmentDto, Department> dtoToDepartment = dto -> Department.builder()
            .departmentId(departmentRepository.getDepartmentIdByDepartmentName(dto.getDepartmentName()))
            .departmentHead(lecturerRepository.getLecturerIdByLecturerEmail(dto.getLecturerEmail()))
            .faculty(facultyRepository.findByFacultyName(dto.getFacultyName()).orElseThrow(NotFoundException::new))
            .departmentName(dto.getDepartmentName())
            .build();

    public List<DepartmentDto> list() {
        List<DepartmentDto> response = new LinkedList<>();
        departmentRepository.findAll().forEach(department -> response.add(departmentToDto.apply(department)));
        return response;
    }

    public DepartmentDto create(DepartmentDto departmentDto) {
        DepartmentDto response = departmentToDto.apply(departmentRepository.save(dtoToDepartment.apply(departmentDto)));
        return response;
    }

    public List<DepartmentDto> find(String departmentName, String firstName, String facultyName){
        List<DepartmentDto> response = new LinkedList<>();
        Lecturer lecturer = lecturerRepository.findByFirstName(firstName).orElse(null);
        Faculty faculty = facultyRepository.findByFacultyName(facultyName).orElse(null);
        departmentRepository.findAll(DepartmentSpecial.buildSearchSpec(departmentName, lecturer, faculty)).forEach(department -> response.add(departmentToDto.apply(department)));
        return response;
    }

    public void update(DepartmentDto departmentDto) {
        Department department = departmentRepository.findDepartmentByDepartmentName(departmentDto.getDepartmentName())
                .orElseThrow(() -> NotFoundException.notFoundWhenUpdate(Department.class));
        department.setDepartmentHead(lecturerRepository.getLecturerIdByLecturerEmail(departmentDto.getLecturerEmail()));
        departmentRepository.save(department);
    }

    public void delete(DepartmentDto departmentDto) {
        Department department = departmentRepository.findDepartmentByDepartmentName(departmentDto.getDepartmentName())
                .orElseThrow(() -> NotFoundException.notFoundWhenDelete(Department.class));
        departmentRepository.delete(department);
    }
}
