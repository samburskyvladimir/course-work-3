package ua.edu.onu.DepartmentPublications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.onu.DepartmentPublications.dao.model.Faculty;
import ua.edu.onu.DepartmentPublications.dao.repository.DepartmentRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.FacultyRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.special.FacultySpecial;
import ua.edu.onu.DepartmentPublications.dto.FacultyDto;
import ua.edu.onu.DepartmentPublications.dto.FacultyDtoSpec;
import ua.edu.onu.DepartmentPublications.dto.LecturerDtoSpec2;
import ua.edu.onu.DepartmentPublications.exception.NotFoundException;

import javax.swing.text.View;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
public class FacultyService {

    private FacultyRepository facultyRepository;
    private DepartmentRepository departmentRepository;


    @Autowired
    public FacultyService(FacultyRepository facultyRepository, DepartmentRepository departmentRepository) {
        this.facultyRepository = facultyRepository;
        this.departmentRepository = departmentRepository;
    }

    private final Function<Faculty, FacultyDto> facultyToDto = entity -> FacultyDto.builder()
            .facultyName(entity.getFacultyName())
            .build();

    private final Function<FacultyDto, Faculty> dtoToFaculty = dto -> Faculty.builder()
            .facultyId(facultyRepository.getFacultyIdByFacultyName(dto.getFacultyName()))
            .facultyName(dto.getFacultyName())
            .build();

    public List<FacultyDto> list() {
        List<FacultyDto> response = new LinkedList<>();
        facultyRepository.findAll().forEach(faculty -> response.add(facultyToDto.apply(faculty)));
        return response;
    }

    public FacultyDto create(FacultyDto facultyDto) {
        FacultyDto response = facultyToDto.apply(facultyRepository.save(dtoToFaculty.apply(facultyDto)));
        return response;
    }

    public List<FacultyDto> find(String facultyName){
        List<FacultyDto> response = new LinkedList<>();
        facultyRepository.findAll(FacultySpecial.buildSearchSpec(facultyName)).forEach(faculty -> response.add(facultyToDto.apply(faculty)));
        return response;
    }

    public void delete(FacultyDto facultyDto) {
        Faculty faculty = facultyRepository.findByFacultyName(facultyDto.getFacultyName())
                .orElseThrow(() -> NotFoundException.notFoundWhenDelete(Faculty.class));
        facultyRepository.delete(faculty);
    }

   public List<FacultyDtoSpec> getDepartmentsWithLessNumberOfSciencePublications(){
       List<String> data = facultyRepository.getDepartmentsWithLessNumberOfSciencePublications();
       List<FacultyDtoSpec> objects = new LinkedList<>();
       data.forEach(item -> {
           String[] elements = item.split(",");
           objects.add(new FacultyDtoSpec(
                   elements[0],
                   elements[1]
           ));
       });
       return objects;
   }

}
