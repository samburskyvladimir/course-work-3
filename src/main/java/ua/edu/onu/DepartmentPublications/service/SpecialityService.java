package ua.edu.onu.DepartmentPublications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.onu.DepartmentPublications.dao.model.Speciality;
import ua.edu.onu.DepartmentPublications.dao.model.Subject;
import ua.edu.onu.DepartmentPublications.dao.repository.SpecialityRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.SubjectRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.special.SpecialitySpecial;
import ua.edu.onu.DepartmentPublications.dto.SpecialityDto;
import ua.edu.onu.DepartmentPublications.exception.NotFoundException;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
public class SpecialityService {

    private SpecialityRepository specialityRepository;
    private SubjectRepository subjectRepository;

    @Autowired
    public SpecialityService(SpecialityRepository specialityRepository, SubjectRepository subjectRepository) {
        this.specialityRepository = specialityRepository;
        this.subjectRepository = subjectRepository;
    }

    private final Function<Speciality, SpecialityDto> specialityToDto = entity -> SpecialityDto.builder()
            .specialityName(entity.getSpecialityName())
            .build();

    private final Function<SpecialityDto, Speciality> dtoToSpeciality = dto -> Speciality.builder()
            .specialityId(specialityRepository.getSpecialityIdBySpecialityName(dto.getSpecialityName()))
            .specialityName(dto.getSpecialityName())
            .build();

    public List<SpecialityDto> list() {
        List<SpecialityDto> response = new LinkedList<>();
        specialityRepository.findAll().forEach(speciality -> response.add(specialityToDto.apply(speciality)));
        return response;
    }

    public SpecialityDto create(SpecialityDto specialityDto) {
        SpecialityDto response = specialityToDto.apply(specialityRepository.save(dtoToSpeciality.apply(specialityDto)));
        return response;
    }

    public List<SpecialityDto> find(String specialityName, String subjectName){
        List<SpecialityDto> response = new LinkedList<>();
        Subject subject = subjectRepository.findBySubjectName(subjectName).orElse(null);
        specialityRepository.findAll(SpecialitySpecial.buildSearchSpec(specialityName, subject)).forEach(speciality -> response.add(specialityToDto.apply(speciality)));
        return response;
    }

    public void update(SpecialityDto specialityDto) {
        Speciality speciality = specialityRepository.findSpecialityBySpecialityName(specialityDto.getSpecialityName())
                .orElseThrow(() -> NotFoundException.notFoundWhenUpdate(Speciality.class));
        speciality.setSpecialityName(specialityDto.getSpecialityName());
        specialityRepository.save(speciality);
    }

    public void delete(SpecialityDto specialityDto) {
        Speciality speciality = specialityRepository.findSpecialityBySpecialityName(specialityDto.getSpecialityName())
                .orElseThrow(() -> NotFoundException.notFoundWhenDelete(Speciality.class));
        specialityRepository.delete(speciality);
    }

}
