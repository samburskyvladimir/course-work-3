package ua.edu.onu.DepartmentPublications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.onu.DepartmentPublications.dao.model.*;
import ua.edu.onu.DepartmentPublications.dao.repository.DepartmentRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.LecturerRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.special.LecturerSpecial;
import ua.edu.onu.DepartmentPublications.dto.LecturerDto;
import ua.edu.onu.DepartmentPublications.dto.LecturerDtoSpec1;
import ua.edu.onu.DepartmentPublications.dto.LecturerDtoSpec2;
import ua.edu.onu.DepartmentPublications.dto.LecturerDtoSpec3;
import ua.edu.onu.DepartmentPublications.exception.NotFoundException;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
public class LecturerService {

    private LecturerRepository lecturerRepository;
    private DepartmentRepository departmentRepository;

    @Autowired
    public LecturerService(LecturerRepository lecturerRepository, DepartmentRepository departmentRepository) {
        this.lecturerRepository = lecturerRepository;
        this.departmentRepository = departmentRepository;
    }

    private final Function<Lecturer, LecturerDto> lecturerToDto = entity -> LecturerDto.builder()
            .lecturerId(entity.getLecturerId())
            .departmentName(departmentRepository.getDepartmentNameByDepartmentId(entity.getDepartment().getDepartmentId()))
            .email(entity.getEmail())
            .firstName(entity.getFirstName())
            .lastName(entity.getLastName())
            .patronymic(entity.getPatronymic())
            .post(entity.getPost())
            .rank(entity.getRank())
            .build();

    private final Function<LecturerDto, Lecturer> dtoToLecturer = dto -> Lecturer.builder()
            .lecturerId(dto.getLecturerId())
            .department(departmentRepository.findDepartmentByDepartmentName(dto.getDepartmentName()).orElseThrow( ()-> NotFoundException.returnNotFoundEntity(dto, Faculty.class.getName())))
            .email(dto.getEmail())
            .firstName(dto.getFirstName())
            .lastName(dto.getLastName())
            .patronymic(dto.getPatronymic())
            .post(dto.getPost())
            .rank(dto.getRank())
            .build();
    public List<LecturerDto> list() {
        List<LecturerDto> response = new LinkedList<>();
        lecturerRepository.findAll().forEach(lecturer -> response.add(lecturerToDto.apply(lecturer)));
        return response;
    }

    public LecturerDto create(LecturerDto lecturerDto) {
        LecturerDto response = lecturerToDto.apply(lecturerRepository.save(dtoToLecturer.apply(lecturerDto)));
        return response;
    }

    public List<LecturerDto> find(String lecturerName, String lecturerSurname, String departmentName, String lecturerPost, String lecturerRank){
        List<LecturerDto> response = new LinkedList<>();
        Department department = departmentRepository.findDepartmentByDepartmentName(departmentName).orElse(null);
        lecturerRepository.findAll(LecturerSpecial.buildSearchSpec(lecturerName, lecturerSurname, department, lecturerPost, lecturerRank)).forEach(lecturer -> response.add(lecturerToDto.apply(lecturer)));
        return response;
    }

    public void update(LecturerDto lecturerDto) {
        Lecturer lecturer = lecturerRepository.findById(lecturerDto.getLecturerId())
                .orElseThrow(() -> NotFoundException.notFoundWhenUpdate(Lecturer.class));
        lecturer.setLecturerId(lecturerDto.getLecturerId());
        lecturer.setLastName(lecturerDto.getLastName());
        lecturer.setFirstName(lecturerDto.getFirstName());
        lecturer.setPatronymic(lecturerDto.getPatronymic());
        lecturer.setDepartment(departmentRepository.findDepartmentByDepartmentName(lecturerDto.getDepartmentName()).orElseThrow(()
                -> NotFoundException.returnNotFoundEntity(lecturerDto, Department.class.getName())));
        lecturer.setEmail(lecturerDto.getEmail());
        lecturer.setPost(lecturerDto.getPost());
        lecturer.setRank(lecturerDto.getRank());
        lecturerRepository.save(lecturer);
    }

    public void delete(LecturerDto lecturerDto) {
        lecturerRepository.findById(lecturerDto.getLecturerId())
                .orElseThrow(() -> NotFoundException.notFoundWhenDelete(Lecturer.class));
        lecturerRepository.deleteById(lecturerDto.getLecturerId());
    }

    public  List<LecturerDtoSpec1> findSimilarSurnamesOnFaculty(String facultyName){
        List<String> data = lecturerRepository.getLecturerWithSimilarSurnamesOnFaculty(facultyName);
        List<LecturerDtoSpec1> objects = new LinkedList<>();
        data.forEach(item -> {
            String[] elements = item.split(",");
            objects.add(new LecturerDtoSpec1(
                    elements[0]
                //    elements[1],
                //    elements[2],
                //    elements[3],
                //    elements[4]
            ));
        });
        return objects;
    }

    public List<LecturerDtoSpec2> findLecturerPublicationsForLastTwoYearsByDepartment(String departmentName){
        List<String> data = lecturerRepository.getLecturersInRangePublicationsGeneralAndTeachingPublications(departmentName);
        List<LecturerDtoSpec2> objects = new LinkedList<>();
        data.forEach(item -> {
            String[] elements = item.split(",");
            objects.add(new LecturerDtoSpec2(
                    elements[0],
                    elements[1],
                    elements[2],
                    elements[3],
                    elements[4],
                    elements[5]
            ));
        });
        return objects;
    }

    public List<LecturerDtoSpec3> findLecturersWithBiggerNumbOfSciencePublications(){
        List<String> data = lecturerRepository.getLecturersHaveSciencePublicationsNumbTwiceBiggerThanTeachingPublications();
        List<LecturerDtoSpec3> objects = new LinkedList<>();
        data.forEach(item -> {
            String[] elements = item.split(",");
            objects.add(new LecturerDtoSpec3(
                    elements[0],
                    elements[1],
                    elements[2],
                    elements[3],
                    elements[4]
            ));
        });
        return objects;
    }

}
