package ua.edu.onu.DepartmentPublications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.onu.DepartmentPublications.dao.model.Lecturer;
import ua.edu.onu.DepartmentPublications.dao.model.Publication;
import ua.edu.onu.DepartmentPublications.dao.model.Subject;
import ua.edu.onu.DepartmentPublications.dao.repository.LecturerRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.PublicationRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.SubjectRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.special.PublicationSpecial;
import ua.edu.onu.DepartmentPublications.dto.FacultyDtoSpec;
import ua.edu.onu.DepartmentPublications.dto.PublicationDto;
import ua.edu.onu.DepartmentPublications.dto.PublicationDtoSpec;
import ua.edu.onu.DepartmentPublications.exception.NotFoundException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
@Transactional
public class PublicationService {

    private PublicationRepository publicationRepository;
    private LecturerRepository lecturerRepository;
    private SubjectRepository subjectRepository;

    @Autowired
    public PublicationService(PublicationRepository publicationRepository, LecturerRepository lecturerRepository, SubjectRepository subjectRepository) {
        this.publicationRepository = publicationRepository;
        this.lecturerRepository = lecturerRepository;
        this.subjectRepository = subjectRepository;
    }

    private final Function<Publication, PublicationDto> publicationToDto = entity -> PublicationDto.builder()
            .publicationId(entity.getPublicationId())
            .downloadLink(entity.getDownloadLink())
            .isbnNumb(entity.getIsbnNumb())
            .pageNumb(entity.getPageNumb())
            .publicationCity(entity.getPublicationCity())
            .publicationDate(entity.getPublicationDate())
            .publicationTitle(entity.getPublicationTitle())
            .publicationType(entity.getPublicationType())
            .publicationUse(entity.getPublicationUse())
            .udcNumb(entity.getUdcNumb())
            .subjectName(subjectRepository.getSubjectNameBySubjectId(entity.getSubject().getSubjectId()))
            .build();

    private final Function<PublicationDto, Publication> dtoToPublication = dto -> Publication.builder()
            .publicationId(dto.getPublicationId())
            .downloadLink(dto.getDownloadLink())
            .isbnNumb(dto.getIsbnNumb())
            .pageNumb(dto.getPageNumb())
            .publicationCity(dto.getPublicationCity())
            .publicationDate(dto.getPublicationDate())
            .publicationTitle(dto.getPublicationTitle())
            .publicationType(dto.getPublicationType())
            .publicationUse(dto.getPublicationUse())
            .udcNumb(dto.getUdcNumb())
            .subject(subjectRepository.findBySubjectName(dto.getSubjectName()).orElseThrow(NotFoundException::new))
            .build();

    public List<PublicationDto> list() {
        List<PublicationDto> response = new LinkedList<>();
        publicationRepository.findAll().forEach(publication -> response.add(publicationToDto.apply(publication)));
        return response;
    }

    public PublicationDto create(PublicationDto publicationDto) {
        PublicationDto response = publicationToDto.apply(publicationRepository.save(dtoToPublication.apply(publicationDto)));
        return response;
    }

    public List<PublicationDto> find(Date begin, Date end, String city, String use, String title, Long numbBegin, Long numbEnd, String type, String lecturerSurname, String subjectName){
        List<PublicationDto> response = new LinkedList<>();
        Lecturer  lecturer= lecturerRepository.findByLastName(lecturerSurname).orElse(null);
        Subject subject = subjectRepository.findBySubjectName(subjectName).orElse(null);
        publicationRepository.findAll(PublicationSpecial.buildSearchSpec(begin, end, city, use, title, numbBegin, numbEnd, type, lecturer, subject)).forEach(publication -> response.add(publicationToDto.apply(publication)));
    return response;
    }

    public void update(PublicationDto publicationDto) {
        Publication publication = publicationRepository.findById(publicationDto.getPublicationId())
                .orElseThrow(() -> NotFoundException.notFoundWhenUpdate(Publication.class));
        publication.setPublicationId(publicationDto.getPublicationId());
        publication.setDownloadLink(publicationDto.getDownloadLink());
        publication.setIsbnNumb(publicationDto.getIsbnNumb());
        publication.setPageNumb(publicationDto.getPageNumb());
        publication.setPublicationCity(publicationDto.getPublicationCity());
        publication.setPublicationDate(publicationDto.getPublicationDate());
        publication.setPublicationTitle(publicationDto.getPublicationTitle());
        publication.setPublicationType(publicationDto.getPublicationType());
        publication.setPublicationUse(publicationDto.getPublicationUse());
        publication.setUdcNumb(publicationDto.getUdcNumb());
        publicationRepository.save(publication);
    }

    public void delete(PublicationDto publicationDto) {
        publicationRepository.findById(publicationDto.getPublicationId())
                .orElseThrow(() -> NotFoundException.notFoundWhenDelete(Publication.class));
        publicationRepository.deleteById(publicationDto.getPublicationId());
    }

    public void deleteDateBetweenAndPageNumb(Date begin, Date end, Integer numb){
        publicationRepository.deleteByPublicationDateBetweenAndPageNumbIsLessThan(begin, end, numb);
    }

    public List<PublicationDtoSpec> getMethodicalPublicationsForEachSubjectDesc(){
        List<String> data = publicationRepository.getMethodicalPublicationsForEachSubjectDesc();
        List<PublicationDtoSpec> objects = new LinkedList<>();
        data.forEach(item -> {
            String[] elements = item.split(",");
            objects.add(new PublicationDtoSpec(
                    elements[0],
                    elements[1]
            ));
        });
        return objects;
    }

}
