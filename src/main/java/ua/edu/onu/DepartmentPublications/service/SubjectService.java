package ua.edu.onu.DepartmentPublications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.onu.DepartmentPublications.dao.model.Subject;
import ua.edu.onu.DepartmentPublications.dao.repository.SpecialityRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.SubjectRepository;
import ua.edu.onu.DepartmentPublications.dao.repository.special.SubjectSpecial;
import ua.edu.onu.DepartmentPublications.dto.SubjectDto;
import ua.edu.onu.DepartmentPublications.exception.NotFoundException;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@Service
public class SubjectService {

    private SubjectRepository subjectRepository;
    private SpecialityRepository specialityRepository;

    @Autowired
    public SubjectService(SubjectRepository subjectRepository,SpecialityRepository specialityRepository) {
        this.subjectRepository = subjectRepository;
        this.specialityRepository = specialityRepository;
    }

    private final Function<Subject, SubjectDto> subjectToDto = entity -> SubjectDto.builder()
            .subjectName(entity.getSubjectName())
            .specialityName(specialityRepository.getSpecialityNameBySpecialityId(entity.getSpeciality().getSpecialityId()))
            .build();

    private final Function<SubjectDto, Subject> dtoToSubject = dto -> Subject.builder()
            .subjectId(subjectRepository.getSubjectIdBySubjectName(dto.getSubjectName()))
            .subjectName(dto.getSubjectName())
            .speciality(specialityRepository.findSpecialityBySpecialityName(dto.getSpecialityName()).orElseThrow(NotFoundException::new))
            .build();

    public List<SubjectDto> list() {
        List<SubjectDto> response = new LinkedList<>();
        subjectRepository.findAll().forEach(subject -> response.add(subjectToDto.apply(subject)));
        return response;
    }

    public SubjectDto create(SubjectDto subjectDto) {
        SubjectDto response = subjectToDto.apply(subjectRepository.save(dtoToSubject.apply(subjectDto)));
        return response;
    }

    public List<SubjectDto> find(String subjectName){
        List<SubjectDto> response = new LinkedList<>();
        subjectRepository.findAll(SubjectSpecial.buildSearchSpec(subjectName)).forEach(subject -> response.add(subjectToDto.apply(subject)));
        return response;
    }

    public void update(SubjectDto subjectDto) {
        Subject subject = subjectRepository.findBySubjectName(subjectDto.getSubjectName())
                .orElseThrow(() -> NotFoundException.notFoundWhenUpdate(Subject.class));
        subject.setSubjectName(subjectDto.getSubjectName());
        subjectRepository.save(subject);
    }

    public void delete(SubjectDto subjectDto) {
        Subject subject = subjectRepository.findBySubjectName(subjectDto.getSubjectName())
                .orElseThrow(() -> NotFoundException.notFoundWhenUpdate(Subject.class));
        subjectRepository.delete(subject);
    }

}
