package ua.edu.onu.DepartmentPublications.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PublicationLecturerDto {

    private Long publicationId;

    private String publicationTitle;

    private String publicationUse;

    private String publicationType;

    private String publicationSubject;

    private String departmentName;

    private String lecturerSurname;

    private String lecturerName;

    private String lecturerPatronymic;

    private String lecturerEmail;
}
