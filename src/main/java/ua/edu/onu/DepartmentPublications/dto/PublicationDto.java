package ua.edu.onu.DepartmentPublications.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PublicationDto {

    private Long publicationId;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date publicationDate;

    private String publicationCity;

    private String downloadLink;

    private String publicationUse;

    private String publicationType;

    private Integer udcNumb;

    private Integer isbnNumb;

    private Integer pageNumb;

    private String publicationTitle;

    private String subjectName;
}
