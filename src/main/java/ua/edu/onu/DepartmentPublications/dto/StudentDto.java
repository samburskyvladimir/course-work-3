package ua.edu.onu.DepartmentPublications.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {

    private Long studentId;

    private String firstName;

    private String lastName;

    private String patronymic;

    private String specialityName;

    private String email;

    private String telNo;

    private String additional;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date studyBegin;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date studyEnd;
}
