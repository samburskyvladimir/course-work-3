package ua.edu.onu.DepartmentPublications.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LecturerDtoSpec1 {

    private String last_name;

    //private String first_name;

   // private String patronymic;

   // private String faculty_name;

   // private String department_name;
}
