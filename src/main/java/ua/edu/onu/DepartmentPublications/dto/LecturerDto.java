package ua.edu.onu.DepartmentPublications.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LecturerDto {

    private Long lecturerId;

    private String firstName;

    private String lastName;

    private String patronymic;

    private String email;

    private String post;

    private String rank;

    private String departmentName;
}
