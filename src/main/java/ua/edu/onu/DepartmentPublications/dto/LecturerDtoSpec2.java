package ua.edu.onu.DepartmentPublications.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LecturerDtoSpec2 {

    private String last_name;

    private String publication_title;

    private String publication_date;

    private String publication_use;

    private String rank1;

    private String rank2;
}
