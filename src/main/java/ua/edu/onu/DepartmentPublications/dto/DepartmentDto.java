package ua.edu.onu.DepartmentPublications.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentDto {


    private String departmentName;

    private String lecturerLastName;

    private String lecturerFirstName;

    private String lecturerPatronymic;

    private String lecturerEmail;

    private String facultyName;
}
