package ua.edu.onu.DepartmentPublications.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.edu.onu.DepartmentPublications.dto.SubjectDto;
import ua.edu.onu.DepartmentPublications.service.SubjectService;

import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api")
public class SubjectController {

    private final SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @GetMapping("/subjects/main")
    public String list(Model model) {
        List<SubjectDto> response = subjectService.list();
        model.addAttribute("title", "Предметы");
        model.addAttribute("response", response);
        log.info("GET all subjects: {}", response);
        return "subjects-main";
    }

    @GetMapping("/subjects/create")
    public String createPage(Model model) {
        return "subjects-create";
    }

    @PostMapping("/subjects/create")
    public String create(@ModelAttribute SubjectDto request, Model model) {
        SubjectDto response = subjectService.create(request);
        log.info("CREATE one subject: {}", response);
        return "redirect:/api/subjects/main";
    }

    @GetMapping("/subjects/findBy")
    public List<SubjectDto> get(@RequestParam(required = false) String subjectName) {
        List<SubjectDto> response = subjectService.find(subjectName);
        log.info("GET subjects by params: {}", response);
        return response;
    }

    @PutMapping("/subjects")
    @ResponseStatus(code = HttpStatus.OK)
    public void update(@RequestBody SubjectDto request) {
        subjectService.update(request);
        log.info("UPDATE one subject");
    }

    @DeleteMapping("/subjects")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@RequestBody SubjectDto request) {
        subjectService.delete(request);
        log.info("DELETE one subject");
    }

}
