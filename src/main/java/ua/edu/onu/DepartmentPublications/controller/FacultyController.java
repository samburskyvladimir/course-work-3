package ua.edu.onu.DepartmentPublications.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.edu.onu.DepartmentPublications.dto.FacultyDto;
import ua.edu.onu.DepartmentPublications.dto.FacultyDtoSpec;
import ua.edu.onu.DepartmentPublications.service.FacultyService;

import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api")
public class FacultyController {

    private final FacultyService facultyService;

    @Autowired
    public FacultyController(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @GetMapping("/faculties/main")
    public String list(Model model) {
        List<FacultyDto> response = facultyService.list();
        model.addAttribute("title", "Факультеты университета");
        model.addAttribute("response", response);
        log.info("GET all faculties: {}", response);
        return "faculties-main";
    }

    @GetMapping("/faculties/create")
    public String createPage(Model model) {
        return "faculties-create";
    }


    @PostMapping("/faculties/create")
    public String create(@ModelAttribute FacultyDto request, Model model) {
        FacultyDto response = facultyService.create(request);
        log.info("CREATE one faculty: {}", response);
        return "redirect:/api/faculties/main";
    }

    @GetMapping("/faculties/findBy")
    public List<FacultyDto> get(@RequestParam(required = false) String facultyName){
        List<FacultyDto> response = facultyService.find(facultyName);
        log.info("GET faculties by params: {}", response);
        return response;
    }

    @DeleteMapping("/faculties")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@RequestBody FacultyDto request) {
        facultyService.delete(request);
        log.info("DELETE one faculty");
    }

    @GetMapping("/faculties/getLessNumbOfSciencePublications")
    public List<FacultyDtoSpec> getLessNumbOfSciencePublications(){
        List<FacultyDtoSpec> response = facultyService.getDepartmentsWithLessNumberOfSciencePublications();
        log.info("GET faculties by params: {}", response);
        return response;
    }

}
