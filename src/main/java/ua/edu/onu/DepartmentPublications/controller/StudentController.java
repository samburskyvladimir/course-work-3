package ua.edu.onu.DepartmentPublications.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.edu.onu.DepartmentPublications.dto.StudentDto;
import ua.edu.onu.DepartmentPublications.service.StudentService;

import java.util.Date;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/students/main")
    public String list(Model model) {
        List<StudentDto> response = studentService.list();
        model.addAttribute("title", "Студенты университета");
        model.addAttribute("response", response);
        log.info("GET all students: {}", response);
        return "students-main";
    }

    @GetMapping("/students/create")
    public String createPage(Model model) {
        return "students-create";
    }

    @PostMapping("/students/create")
    public String create(@ModelAttribute StudentDto request, Model model) {
        StudentDto response = studentService.create(request);
        log.info("CREATE one student: {}", response);
        return "redirect:/api/students/main";
    }

    @GetMapping("/students/findBy")
    public List<StudentDto> get(@RequestParam(required = false) String studentName,
                                @RequestParam(required = false) String studentSurname,
                                @RequestParam(required = false) String specialityName,
                                @RequestParam(required = false) String additional,
                                @RequestParam(required = false) String email,
                                @RequestParam(required = false) Date begin,
                                @RequestParam(required = false) Date end) {
        List<StudentDto> response = studentService.find(studentName, studentSurname, specialityName, additional, email, begin, end);
        log.info("GET students by params: {}", response);
        return response;
    }

    @PutMapping("/students")
    @ResponseStatus(code = HttpStatus.OK)
    public void update(@RequestBody StudentDto request) {
        studentService.update(request);
        log.info("UPDATE one student");
    }

    @DeleteMapping("/students")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@RequestBody StudentDto request) {
        studentService.delete(request);
        log.info("DELETE one student");
    }
}
