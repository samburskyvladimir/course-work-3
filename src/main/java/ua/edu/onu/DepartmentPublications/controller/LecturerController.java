package ua.edu.onu.DepartmentPublications.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.edu.onu.DepartmentPublications.dto.LecturerDto;
import ua.edu.onu.DepartmentPublications.dto.LecturerDtoSpec1;
import ua.edu.onu.DepartmentPublications.dto.LecturerDtoSpec2;
import ua.edu.onu.DepartmentPublications.dto.LecturerDtoSpec3;
import ua.edu.onu.DepartmentPublications.service.LecturerService;

import javax.swing.text.View;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api")
public class LecturerController {

    private final LecturerService lecturerService;

    @Autowired
    public LecturerController(LecturerService lecturerService) {
        this.lecturerService = lecturerService;
    }

    @GetMapping("/lecturers/main")
    public String list(Model model) {
        List<LecturerDto> response = lecturerService.list();
        model.addAttribute("title", "Преподаватели кафедры");
        model.addAttribute("response", response);
        log.info("GET all lecturers: {}", response);
        return "lecturers-main";
    }

    @GetMapping("/lecturers/create")
    public String createPage(Model model) {
        return "lecturers-create";
    }


    @PostMapping("/lecturers/create")
    public String create(@ModelAttribute LecturerDto request, Model model) {
        LecturerDto response = lecturerService.create(request);
        log.info("CREATE one lecturer: {}", response);
        return "redirect:/api/lecturers/main";
    }

    @GetMapping("/lecturers/findBy")
    public List<LecturerDto> get(@RequestParam(required = false) String departmentName,
                                   @RequestParam(required = false) String lecturerName,
                                   @RequestParam(required = false) String lecturerSurname,
                                 @RequestParam(required = false) String post,
                                 @RequestParam(required = false) String rank){
        List<LecturerDto> response = lecturerService.find(departmentName, lecturerName, lecturerSurname, post, rank);
        log.info("GET lecturers by params: {}", response);
        return response;
    }

    @PutMapping("/lecturers")
    @ResponseStatus(code = HttpStatus.OK)
    public void update(@RequestBody LecturerDto request) {
        lecturerService.update(request);
        log.info("UPDATE one lecturer");
    }

    @DeleteMapping("/lecturers")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@RequestBody LecturerDto request) {
        lecturerService.delete(request);
        log.info("DELETE one lecturer");
    }

    @GetMapping("/lecturers/findSimilarSurnamesOnFaculty")
    public List<LecturerDtoSpec1> getSimilarSurnamesOnFaculty(@RequestParam(required = false) String facultyName){
        List<LecturerDtoSpec1> response = lecturerService.findSimilarSurnamesOnFaculty(facultyName);
        log.info("GET lecturers by params: {}", response);
        return response;
    }

    @GetMapping("/lecturers/findPublicationsForTwoYears")
    public List<LecturerDtoSpec2> getPublicationsForLastYears(@RequestParam(required = false) String departmentName){
        List<LecturerDtoSpec2> response = lecturerService.findLecturerPublicationsForLastTwoYearsByDepartment(departmentName);
        log.info("GET lecturers by params: {}", response);
        return response;
    }

    @GetMapping("/lecturers/SciencePublications")
    public List<LecturerDtoSpec3> getLecturersWithSciencePublications(){
        List<LecturerDtoSpec3> response = lecturerService.findLecturersWithBiggerNumbOfSciencePublications();
        log.info("GET lecturers by params: {}", response);
        return response;
   }
}
