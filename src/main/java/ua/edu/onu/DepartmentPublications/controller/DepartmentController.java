package ua.edu.onu.DepartmentPublications.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ua.edu.onu.DepartmentPublications.dto.DepartmentDto;
import ua.edu.onu.DepartmentPublications.service.DepartmentService;

import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api")
public class DepartmentController {

    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/departments/main")
    public String list(Model model) {
        List<DepartmentDto> response = departmentService.list();
        model.addAttribute("title", "Кафедры университета");
        model.addAttribute("response", response);
        log.info("GET all departments: {}", response);
        return "departments-main";
    }

    @GetMapping("/departments/create")
    public String createPage(Model model) {
        return "departments-create";
    }

    @PostMapping("/departments/create")
    public String create(@ModelAttribute DepartmentDto request, Model model) {
        DepartmentDto response = departmentService.create(request);
        log.info("CREATE one department: {}", response);
        return "redirect:/api/departments/main";
    }

    @GetMapping("/departments/findBy")
    public List<DepartmentDto> get(@RequestParam(required = false) String departmentName,
                                   @RequestParam(required = false) String headName,
                                   @RequestParam(required = false) String facultyName){
        List<DepartmentDto> response = departmentService.find(departmentName, headName, facultyName);
        log.info("GET departments by params: {}", response);
        return response;
    }

    @PutMapping("/departments")
    @ResponseStatus(code = HttpStatus.OK)
    public void update(@RequestBody DepartmentDto request) {
        departmentService.update(request);
        log.info("UPDATE one department");
    }

    @DeleteMapping("/departments")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@RequestBody DepartmentDto request) {
        departmentService.delete(request);
        log.info("DELETE one department");
    }
}
