package ua.edu.onu.DepartmentPublications.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.edu.onu.DepartmentPublications.dto.SpecialityDto;
import ua.edu.onu.DepartmentPublications.service.SpecialityService;

import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api")
public class SpecialityController {

    private final SpecialityService specialityService;

    @Autowired
    public SpecialityController(SpecialityService specialityService) {
        this.specialityService = specialityService;
    }

    @GetMapping("/specialities/main")
    public String list(Model model) {
        List<SpecialityDto> response = specialityService.list();
        model.addAttribute("title", "Специальности");
        model.addAttribute("response", response);
        log.info("GET all specialities: {}", response);
        return "specialities-main";
    }


    @GetMapping("/specialities/create")
    public String createPage(Model model) {
        return "specialities-create";
    }

    @PostMapping("/specialities/create")
    public String create(@ModelAttribute SpecialityDto request, Model model) {
        SpecialityDto response = specialityService.create(request);
        log.info("CREATE one speciality: {}", response);
        return "redirect:/api/specialities/main";
    }

    @GetMapping("/specialities/findBy")
    public List<SpecialityDto> get(@RequestParam(required = false) String specialityName,
                                   @RequestParam(required = false) String subjectName) {
        List<SpecialityDto> response = specialityService.find(specialityName, subjectName);
        log.info("GET specialities by params: {}", response);
        return response;
    }

    @PutMapping("/specialities")
    @ResponseStatus(code = HttpStatus.OK)
    public void update(@RequestBody SpecialityDto request) {
        specialityService.update(request);
        log.info("UPDATE one speciality");
    }

    @DeleteMapping("/specialities")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@RequestBody SpecialityDto request) {
        specialityService.delete(request);
        log.info("DELETE one speciality");
    }

}
