package ua.edu.onu.DepartmentPublications.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.edu.onu.DepartmentPublications.dto.PublicationLecturerDto;
import ua.edu.onu.DepartmentPublications.service.PublicationLecturerService;

import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api")
public class PublicationLecturerController {

    private final PublicationLecturerService publicationLecturerService;

    @Autowired
    public PublicationLecturerController(PublicationLecturerService publicationLecturerService) {
        this.publicationLecturerService = publicationLecturerService;
    }

    @GetMapping("/publicationLecturers/main")
    public String list(Model model) {
        List<PublicationLecturerDto> response = publicationLecturerService.list();
        model.addAttribute("title", "Авторство");
        model.addAttribute("response", response);
        log.info("GET all publication-lecturer link: {}", response);
        return "publicationLecturers-main";
    }

    @GetMapping("/publicationLecturers/create")
    public String createPage(Model model) {
        return "publicationLecturers-create";
    }

    @PostMapping("/publicationLecturers/create")
    public String create(@ModelAttribute PublicationLecturerDto request, Model model) {
        PublicationLecturerDto response = publicationLecturerService.create(request);
        log.info("CREATE one publication-lecturer link: {}", response);
        return "redirect:/api/publicationLecturers/main";
    }

    @GetMapping("/publicationLecturers/findBy")
    public List<PublicationLecturerDto> get(@RequestParam(required = false) String lecturerSurname,
                                       @RequestParam(required = false) String publicationTitle) {
        List<PublicationLecturerDto> response = publicationLecturerService.find(lecturerSurname, publicationTitle);
        log.info("GET publication-lecturer links by params: {}", response);
        return response;
    }
}
