package ua.edu.onu.DepartmentPublications.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.edu.onu.DepartmentPublications.dto.PublicationDto;
import ua.edu.onu.DepartmentPublications.dto.PublicationDtoSpec;
import ua.edu.onu.DepartmentPublications.service.PublicationService;

import java.util.Date;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api")
public class PublicationController {

    private final PublicationService publicationService;

    @Autowired
    public PublicationController(PublicationService publicationService) {
        this.publicationService = publicationService;
    }

    @GetMapping("/publications/main")
    public String list(Model model) {
        List<PublicationDto> response = publicationService.list();
        model.addAttribute("title", "Публикации");
        model.addAttribute("response", response);
        log.info("GET all publications: {}", response);
        return "publications-main";
    }

    @GetMapping("/publications/create")
    public String createPage(Model model) {
        return "publications-create";
    }

    @PostMapping("/publications/create")
    public String create(@ModelAttribute PublicationDto request, Model model) {
        PublicationDto response = publicationService.create(request);
        log.info("CREATE one publication: {}", response);
        return "redirect:/api/publications/main";
    }

    @GetMapping("/publications/findBy")
    public List<PublicationDto> get(@RequestParam(required = false) Date begin,
                                    @RequestParam(required = false) Date end,
                                    @RequestParam(required = false) String city,
                                    @RequestParam(required = false) String use,
                                    @RequestParam(required = false) String title,
                                    @RequestParam(required = false) Long numbBegin,
                                    @RequestParam(required = false) Long numbEnd,
                                    @RequestParam(required = false) String type,
                                    @RequestParam(required = false) String lecturerSurname,
                                                                    String subjectName) {
        List<PublicationDto> response = publicationService.find(begin, end, city, use, title, numbBegin, numbEnd, type, lecturerSurname, subjectName);
        log.info("GET publications by params: {}", response);
        return response;
    }

    @PutMapping("/publications")
    @ResponseStatus(code = HttpStatus.OK)
    public void update(@RequestBody PublicationDto request) {
        publicationService.update(request);
        log.info("UPDATE one publication");
    }

    @DeleteMapping("/publications")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@RequestBody PublicationDto request) {
        publicationService.delete(request);
        log.info("DELETE one publication");
    }

    @DeleteMapping("/publications/BetweenAndNumb")
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteDateBetweenAndPageNumb(
            @DateTimeFormat(pattern = "dd-MM-yyyy")
            @RequestParam(required = false) Date begin,
            @DateTimeFormat(pattern = "dd-MM-yyyy")
            @RequestParam(required = false) Date end,
            @RequestParam(required = false) Integer numb) {
        publicationService.deleteDateBetweenAndPageNumb(begin, end, numb);
        log.info("DELETE publications by BetweenAndNumb");
    }

    @GetMapping("/publications/EachForEach")
    public List<PublicationDtoSpec> get() {
        List<PublicationDtoSpec> response = publicationService.getMethodicalPublicationsForEachSubjectDesc();
        log.info("GET publications by params: {}", response);
        return response;
    }

}